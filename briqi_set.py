#!/usr/bin/python
# Vlad Negnevitsky
#
# Changelog
# Version 1.2, 12.01.15 : added extra parameter for timeouts (reading from file only)
# Version 1.1, 25.06.13 : turned off sinc filter, set aux dac to 0xff


import sys
import serial, struct, platform, time, glob

# Constants
WR_LSB = 1
WR_CSB = 2
WR_MSB = 4
SET_VGA = 8

# Parameters (globals)
port = None
chan = 1
prof = 0
freq = 0x0
amp = 0x0
ph = 0x0
vga_amp = 0x3fff
timeout_rd = 5
timeout_wr = 1

baudrate = 12000000

# Basic functions
def resync():
    # Writes byte by byte until a read takes place - then presumably
    # comms are resynced
    try:
        port.read(1000) # flush port
    except:
        while(1):
            port.write(struct.pack('>B',0))
            try:
                if (port.read(1) == 0):
                    break
            except:
                pass

def send_wd(wd):
    bs = struct.pack('>I',
                     wd)
    port.write(bs)
    try:
        ret = struct.unpack('>B',port.read(1))[0]
    except:
        raise IOError("serial read failed or timed out")

    # print("returned: ",hex(ret & 0xff)," sent: ",hex(wd))
    if (ret & 0xff) != ( (wd >> 24) & 0xff ):
        raise IOError("serial readback failed - corruption probable.")
    
def wr_op(opcode, payload):
    global chan
    send_wd( (((chan-1) & 0x3) << 30) | (opcode << 24) | payload)

def set_vga(dds_tx_length):
    global chan
    send_wd( (((chan-1) & 0x3) << 30) | (SET_VGA << 24) | ((dds_tx_length & 0x3)<< 16) | (vga_amp & 0x3fff))

def set_freq():
    wr_op(WR_CSB, ((freq >> 24) & 0x0000ff) | ((ph << 8) & 0xffff00))
    wr_op(WR_LSB,freq & 0xffffff)
    set_vga(3)
    set_vga(0)    # update

def set_amp():
    global prof
    wr_op(WR_MSB, 0x0e0000 + ( prof<<16 ) | amp)
    set_vga(3)
    set_vga(0)    # update

# def set_phase(chan):
#     # Exactly like set_freq() except without lower 24 bits (they only
#     # contain frequency info, thus are unneeded for phase)
#     wr_op(chan,WR_CSB, ((freq >> 24) & 0x0000ff) | ((ph << 8) & 0xffff00))
#     print('Writing freq',freq,' command',hex(((freq >> 24) & 0x0000ff) | ((ph << 8) & 0xffff00)))
#     set_vga(chan,3)
#     set_vga(chan,0)    # update
    
def init_dds():
    #    import pdb;pdb.set_trace()
    global chan
    assert 0<chan<5, "invalid DDS channel, use 1-4"

    # 0x000040 : sinc on, 0x000000 : sinc off
    wr_op(WR_MSB,0x000000)
    wr_op(WR_CSB,0x000000)
    set_vga(1)
    
    # CFR 2
    wr_op(WR_MSB,0x010140)
    wr_op(WR_CSB,0x088000)
    set_vga(1)

    # CFR 3
    wr_op(WR_MSB,0x021f3f)
    wr_op(WR_CSB,0xc00000)
    set_vga(1)

    # Aux DAC control
    wr_op(WR_MSB,0x030000)
    wr_op(WR_CSB,0x00ff00)
    set_vga(1)
    
    # Clear profile registers
    for k in range(8):
        msb_wd = 0x0e0000 + ( k<<16 )
        wr_op(WR_MSB,msb_wd)
        wr_op(WR_CSB,0x000000)
        wr_op(WR_LSB,0x000000)

    # wr_op(WR_MSB,0x0e3fff)
    # wr_op(WR_CSB,0x000010)
    # wr_op(WR_LSB,0x000000)
    set_vga(3)
    set_vga(0)    # Update
    
def powerdown_dds():
    global chan
    assert 0<chan<5, "invalid DDS channel, use 1-4"
    wr_op(WR_MSB,0x000040)
    wr_op(WR_CSB,0x00f000)
    set_vga(1)
    set_vga(0) # Update

    # Clear profile registers
    for k in range(8):
        msb_wd = 0x0e0000 + ( k<<16 )
        wr_op(WR_MSB,msb_wd)
        wr_op(WR_CSB,0x000000)
        wr_op(WR_LSB,0x000000)
    
def scan_ports():
    system_name = platform.system()
    if system_name == "Windows":
        open_ports = []
        for i in range(256):
            try:
                s = serial.Serial(i, 9600,
                                  timeout = 1,
                                  xonxoff = False,
                                  writeTimeout = 1,
                                  interCharTimeout = 1,
                                  stopbits = 1)
                print("Port ",i+1," is available")
                open_ports.append(i)
                s.close()
            except serial.SerialException:
                pass
        return open_ports
    elif system_name == "Darwin":
        return glob.glob('/dev/tty*') + glob.glob('/dev/cu*')
    else:
        # Assume Linux or something else
        return glob.glob('/dev/ttyS*') + glob.glob('/dev/ttyUSB*')    

def init_all():
    global chan
    for k in range(1,5):
        chan = k
        init_dds()

def powerdown_all():
    global chan
    for k in range(1,5):
        chan = k
        powerdown_dds()        

def open_port(port_num):
    assert 0 < port_num < 257, "invalid serial port number, use 1 - 256"
    global port
    port = serial.Serial(port_num-1, baudrate,
                         timeout = timeout_rd,
                         xonxoff = False,
                         writeTimeout = timeout_wr,
                         interCharTimeout = 1,
                         stopbits = 1)

        
def set_value(channel, param, value, profile=0):

    global freq, ph, vga_amp, amp, chan, prof
    chan = channel
    prof = profile
#    import pdb;pdb.set_trace()
    assert 0 < chan < 5, "invalid DDS channel, use 1 - 4"
    assert 0 <= prof < 8, "invalid DDS profile, use 0 - 7" 
    assert param in ['f','a','v','p'], "invalid parameter, use 'f', 'a', 'v' or 'p'"
    
#    import pdb;pdb.set_trace()

    if (param == 'f' or param == 'F'):
        assert 1 < value < 450, "frequency is outside 1 - 450 MHz"
        freq = int(value * 2**32 /1000)
        set_freq()
    elif (param == 'a' or param == 'A'):
        amp = int(value * 0x3fff / 100)
        assert 0 <= amp <= 0x3fff, "amplitude is outside 0 - 100%"
        set_amp()
    elif (param == 'p' or param == 'P'):
        ph = int(value[1] * 0xffff / 360)
        freq = int(value[0] * 2**32 /1000)
        assert 1 < value[0] < 450, "frequency is outside 1 - 450 MHz"
        assert 0 <= ph <= 0xffff, "phase is outside 0 - 360"
        set_freq() #set_frequency() sets both frequency and phase
    elif (param == 'v' or param == 'V'):
        vga_amp = int(value*0x3fff/100)
        assert 0 <= vga_amp <= 0x3fff, "invalid VGA amplitude, use 0 - 100"
        set_vga(0)

def load_settings(filename):
    with open(filename, "r") as settings_file:
        for full_line in settings_file:
            line = full_line.strip()
            if (len(line) == 0) or (line[0] == '#'): # comment or blank line
                pass 
            else:
                lvals = line.split(' ')
                if lvals[0] == 'port':
                    llv = len(lvals)
                    assert llv == 2 or llv == 4 or llv == 6, "Wrong number of port arguments! See dds_settings_example.txt."
                    if llv >= 4:
                        assert lvals[2] == 'timeout_rd', "Wrong formatting in port line! See dds_settings_example.txt."
                        global timeout_rd
                        timeout_rd = float(lvals[3])
                    
                    if llv == 6:
                        assert lvals[4] == 'timeout_wr', "Wrong formatting in port line! See dds_settings_example.txt."
                        global timeout_wr
                        timeout_wr = float(lvals[5])
                            
                    open_port(int(lvals[1]))
                    init_all()
                else:
                    global prof
                    channel = int(lvals[0])
                    if len(lvals) > 7:
                        prof = int(lvals[7])

#                    import pdb;pdb.set_trace()
                    set_value(channel,
                              lvals[1], # parameter
                              float(lvals[2]), # value
                              prof) # profile
                    set_value(channel,
                              lvals[3], # parameter
                              float(lvals[4]), # value
                              prof) # profile
                    set_value(channel,
                              lvals[5], # parameter
                              float(lvals[6]), # value
                              prof) # profile

if __name__ == "__main__":
    if len(sys.argv) == 1:
        print("Too few arguments. Type briqi_set --help for help.")
    elif sys.argv[1] == "--help":
        print("""This utility programs a DDS board with the BRIQI firmware. A typical workflow
consists of:

    * scanning the ports to find the DDS board's channel,
    * initialising the DDS board,
    * setting parameters to the values you desire.

Usage:

    --help: this message
    -f, --file: open a file (by default dds_settings.txt) and load the settings therein
    -s, --scan: returns a list of open serial ports.
    -i, --init W: initialise all channels on Port W
       You must initialise a powered-on board before setting values.
       Example: briqi_set -i 8
    -v, --value  W X Y Z [P]: opens port W, sets channel X, parameter Y to Z.
        W: number 1 -> 256
        X: 1 -> 4, or a comma-separated list like 1,2,3,4 (no spaces)
        Y: either f [freq, MHz], a [amp, percent], v [vga amp, percent] or p [phase, degrees]
        Z: value of Y
        P: phase (only active for frequency commands)
       Example: To set a DDS board on Port 8, channel 1, amplitude to 100%:
       briqi_set -v 8 1 a 100
    -p, --powerdown W: power down all channels to avoid the board melting.
       Example: briqi_set -p 8

    You must run --init after a powercycle.
""")
    elif sys.argv[1] == "--file" or sys.argv[1] == "-f":
        assert len(sys.argv) <= 3, "incorrect number of input arguments"
        if len(sys.argv) == 3:
            filename = sys.argv[2]
        else:
            filename = "dds_settings.txt"

        load_settings(filename)
    elif sys.argv[1] == "--init" or sys.argv[1] == "-i":
        assert len(sys.argv) == 3, "incorrect number of input arguments"
        open_port(int(sys.argv[2]))
        init_all()
    elif sys.argv[1] == "--powerdown" or sys.argv[1] == "-p":
        assert len(sys.argv) == 3, "incorrect number of input arguments"        
        open_port(int(sys.argv[2]))
        powerdown_all()
    elif sys.argv[1] == "--scan" or sys.argv[1] == "-s":
        scan_ports()
    elif sys.argv[1] == "-v" or sys.argv[1] == "--value":
        assert 5 < len(sys.argv) < 8, "incorrect number of input arguments"
        if len(sys.argv) == 6: # freq, amp or VGA amp
            open_port(int(sys.argv[2]))
            channel_str = sys.argv[3].split(',')
            for k in channel_str:
                set_value(int(k), sys.argv[4], float(sys.argv[5]))
        else: # freq and phase
            open_port(int(sys.argv[2]))
            set_value(int(sys.argv[3]), 'p', [float(sys.argv[5]),float(sys.argv[6])]) # frequency and phase

    elif sys.argv[1] == '-r'or sys.argv[1] == '--ramp':
        import numpy as np
        assert len(sys.argv) == 8, 'incorrect number of input arguments'
        # import pdb;pdb.set_trace()
        assert int(sys.argv[6]) > 2, 'too few points'
        fstart = float(sys.argv[4])
        fstop = float(sys.argv[5])
        farray = np.linspace(fstart, fstop, int(sys.argv[6])) # create freq array
        open_port(int(sys.argv[2]))

        loop_inf = int(sys.argv[7])
        
        def sweep():
            for k in farray:
                print('Current freq: ',k)
                set_value(int(sys.argv[3]), 'f', k)

        sweep()
        while(loop_inf != 1):
            sweep()
            
    else:
        print("Unrecognised command. Type --help for info.")
